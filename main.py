# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

original = {
    'あ': 'あずさ',
    'い': 'いちじていし',
    'う': 'しんごう',
    'え': 'えすえる',
    'お': 'おうだんほどう',
    'か': 'ぱとろーるかー',
    'き': 'きゅうきゅうしゃ',
    'く': 'だんぷとらっく',
    'け': 'けいてきならせ',
    'こ': 'こだま',
    'さ': 'はやぶさ',
    'し': 'しろばい',
    'す': 'すーぱーあんびゅらんす',
    'せ': 'らくせきのおそれあり',
    'そ': 'そにっく',
    'た': 'きんたろう',
    'ち': 'こまち',
    'つ': 'つばめ',
    'て': 'てんくう',
    'と': 'いーすとあい',
    'な': 'すいなんきゅうじょしゃ',
    'に': 'にちりん',
    'ぬ': 'すぺーしあきぬがわ',
    'ね': 'こうふくのまねきねこでんしゃ',
    'の': 'のぞみ',
    'は': 'はしごしゃ',
    'ひ': 'ひのとり',
    'ふ': 'ふみきり',
    'へ': 'へりこぷたー',
    'ほ': 'いなほ',
    'ま': 'まりんらいなー',
    'み': 'ごみしゅうしゅうしゃ',
    'む': 'かむい',
    'め': 'ろめんでんしゃ',
    'も': 'ももたろう',
    'や': 'かがやき',
    'ゆ': 'ゆあつしょべる',
    'よ': 'ばくはつぶつしょりようぐうんぱんしゃ',
    'ら': 'らびゅー',
    'り': 'りょかくき',
    'る': 'ぶるどーざー',
    'れ': 'れすきゅーしゃ',
    'ろ': 'ほいーるろーだー',
    'わ': 'そうわんじゅうき',
    'を': 'ふみきりをつうか',
    'ん': 'ぽんぷしゃ'
}

answer_list = []

def solve():
    sorted_problem = list(original.items())
    sorted_problem.sort(key=lambda kv: sum(( kv[0] in v) for v in original.values()))
    print(sorted_problem)
    kana_list = list(x[0] for x in sorted_problem)
    word_list = list(x[1] for x in sorted_problem)
    dic = {}
    dfs(0, kana_list, word_list, dic)
    answer_list.sort(key = lambda s : s[0])
    for answer in answer_list:
        print(answer)


def dfs(idx, kana_list, word_list, dic):
    l = len(kana_list)
    if idx >= l:
        score(dic)
        return True
    kana = kana_list[idx]
    for i in range(idx, l):
        word = word_list[i]
        if kana in word:
            dic[kana] = word
            word_list[i],word_list[idx] = word_list[idx],word_list[i]
            dfs(idx+1, kana_list, word_list, dic)
            word_list[i], word_list[idx] = word_list[idx], word_list[i]


def score(answer):
    s = 0.0
    total_diff_score = 0.0
    diff_list = []
    for key,value in answer.items():
        ws = word_score(key, value)
        os = word_score(key, original[key])
        s += ws
        total_diff_score += (ws-os)
        if original[key] != value:
            diff_list.append((key, value, original[key], (ws - os)))
    answer_list.append((s, str(diff_list), total_diff_score))


def word_score(kana, word):
    s = 0.0
    for i, c in enumerate(word):
        s += pow(0.5, i) if c == kana else 0
    return s


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    solve()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
